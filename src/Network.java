import java.io.Serializable;
import java.util.Objects;

public class Network implements Serializable {
    private int nodes=0;
    private Address local;
    private Address left;
    private Address sLeft;
    private Address right;
    private Address sRight;

    public int getNodes() {
        return nodes;
    }

    public void setNodes(int nodes) {
        this.nodes = nodes;
    }

    public Address getLocal() {
        return local;
    }

    public void setLocal(Address local) {
        this.local = local;
    }

    public Address getLeft() {
        return left;
    }

    public void setLeft(Address left) {
        this.left = left;
    }

    public Address getsLeft() {
        return sLeft;
    }

    public void setsLeft(Address sLeft) {
        this.sLeft = sLeft;
    }

    public Address getRight() {
        return right;
    }

    public void setRight(Address right) {
        this.right = right;
    }

    public Address getsRight() {
        return sRight;
    }

    public void setsRight(Address sRight) {
        this.sRight = sRight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Network network = (Network) o;
        return nodes == network.nodes &&
                Objects.equals(local, network.local) &&
                Objects.equals(left, network.left) &&
                Objects.equals(sLeft, network.sLeft) &&
                Objects.equals(right, network.right) &&
                Objects.equals(sRight, network.sRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodes, local, left, sLeft, right, sRight);
    }
}
