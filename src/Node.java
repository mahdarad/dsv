import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public final class Node implements Serializable {
    private static Node INSTANCE;
    private Network net = new Network();
    private Sleeper sleeper = new SleeperImpl();


    public static Node getInstance(){
        if (INSTANCE!=null) return INSTANCE;
        synchronized (Node.class) {
            if (INSTANCE==null) INSTANCE=new Node();
            return INSTANCE;
        }
    }

    public Network getNet(){
        return net;
    }

    public Sleeper getRemoteSleeper(Address add) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(add.getIp(),add.getPort());
        Sleeper stub = (Sleeper) registry.lookup("sleeper");
        return stub;
    }

    public void bind() throws RemoteException, AlreadyBoundException {
        Sleeper stub = (Sleeper) UnicastRemoteObject.exportObject(sleeper,0);
        Registry registry = LocateRegistry.createRegistry(net.getLocal().getPort());
        registry.bind("sleeper",stub);
    }

    public void connectTo(Address address) throws RemoteException, NotBoundException {
        final Sleeper sleeper = getRemoteSleeper(address);
        final Network network = sleeper.getNode().getNet();
        net.setNodes(network.getNodes() + 1);
        switch (net.getNodes()){
            case 1:
                // i am the only node
                net.setRight(net.getLocal());
                net.setsRight(net.getLocal());
                net.setLeft(net.getLocal());
                net.setsLeft(net.getLocal());
                break;
            case 2:
                // connecting to a single node
                net.setRight(network.getLocal());
                net.setsRight(net.getLocal());
                net.setLeft(network.getLocal());
                net.setsLeft(net.getLocal());
                sleeper.setLeft(net.getLocal());
                sleeper.setRight(net.getLocal());
                sleeper.addNode();
                break;
            default:
                net.setRight(network.getRight());
                net.setsRight(network.getsRight());
                net.setLeft(network.getLocal());
                net.setsLeft(network.getLeft());
                // sends the ring message to fix others
                Message msg = new Message();
                msg.addStep();
                msg.setType(MessageType.RING);
                msg.setNet(getNet());
                getRemoteSleeper(getNet().getLeft()).onMessage(msg);
                getRemoteSleeper(getNet().getRight()).onMessage(msg);
                msg.addStep();
                getRemoteSleeper(getNet().getsRight()).onMessage(msg);
                break;
        }



    }

    public void disconnect() throws RemoteException, NotBoundException {
        sleeper.disconnect();
    }

}
