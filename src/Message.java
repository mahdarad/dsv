import java.io.Serializable;

public class Message implements Serializable {

    private MessageType type;
    private Network net;
    private int steps = 0;

    public Message() {
        super();
    }

    public Message(MessageType type, Network net) {
        this();
        this.type=type;
        this.net=net;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public Network getNet() {
        return net;
    }

    public void setNet(Network net) {
        this.net = net;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public void addStep(){
        steps++;
    }

}
