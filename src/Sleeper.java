import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Sleeper extends Remote {
    void start(int workload) throws RemoteException, NotBoundException;
    void request(int workload,Address add) throws RemoteException, NotBoundException;
    void share(int workload,Address add) throws RemoteException, NotBoundException;
    void add(int workload) throws RemoteException, NotBoundException;
    void onMessage(Message msg) throws RemoteException, NotBoundException;
    Node getNode() throws  RemoteException;
    void setLeft(Address address) throws RemoteException;
    void setsLeft(Address address) throws RemoteException;
    void setRight(Address address) throws RemoteException;
    void setsRight(Address address) throws RemoteException;
    void fixAndRing() throws RemoteException,NotBoundException;
    void addNode() throws RemoteException,NotBoundException;
    void disconnect() throws RemoteException,NotBoundException;

    void startToken() throws RemoteException, NotBoundException;
}
