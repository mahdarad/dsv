import java.io.Serializable;
import java.util.Objects;

public class Address implements Serializable {
    private String ip;
    private int port;

    public Address(String add) {
        this.ip=add.split(":")[0];
        this.port=Integer.parseInt(add.split(":")[1]);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return port == address.port &&
                Objects.equals(ip, address.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, port);
    }
}
