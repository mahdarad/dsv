import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class SleeperImpl implements Sleeper {
    private int work;
    private boolean black = false;


    @Override
    public void start(int workload) throws RemoteException, NotBoundException {
        if (!black) {
            black = true;
            work = workload;
            Timer t = new Timer(this);
            t.start();
            share(work/2,getNode().getNet().getLeft());
        } else {
            System.out.println("Already working");
        }
    }

    @Override
    public void addNode() throws RemoteException, NotBoundException {
        Node n = Node.getInstance();
        n.getNet().setNodes(n.getNet().getNodes()+1);
    }

    @Override
    public void setLeft(Address address) throws RemoteException {
        Node n = Node.getInstance();
        n.getNet().setLeft(address);
    }

    @Override
    public void setsLeft(Address address) throws RemoteException {
        Node n = Node.getInstance();
        n.getNet().setsLeft(address);
    }

    @Override
    public void setRight(Address address) throws RemoteException {
        Node n = Node.getInstance();
        n.getNet().setRight(address);
    }

    @Override
    public void setsRight(Address address) throws RemoteException {
        Node n = Node.getInstance();
        n.getNet().setsRight(address);
    }

    @Override
    public Node getNode() throws RemoteException{
        return Node.getInstance();
    }

    @Override
    public void add(int workload) throws RemoteException, NotBoundException {
        if (black)work+=workload;
        else {
            black = true;
            work = workload;
            Timer t = new Timer(this);
            t.start();
        }
    }

    @Override
    public void request(int workload,Address add) throws RemoteException, NotBoundException {
        try {
            Node n = Node.getInstance();
            n.getRemoteSleeper(add).share(workload, n.getNet().getLocal());
        }catch (Exception e){
            fixAndRing();
            System.out.println("try again");
        }
    }

    @Override
    public void share(int workload,Address add) throws RemoteException, NotBoundException {
        try {
            Node n = Node.getInstance();
            if (workload<work+1) {
                n.getRemoteSleeper(add).add(workload);
                work -= workload;
            } else {
                System.out.println("Could not share that much");
            }
        } catch (Exception e){
            fixAndRing();
            System.out.println("try again");
        }

    }

    @Override
    public void onMessage(Message msg) throws RemoteException, NotBoundException {
        Network remNetwork = msg.getNet();
        Node localNode = getNode();
        switch (msg.getType()){
            case RING:
                System.out.println("got a ring");
                boolean c = false;
                if (remNetwork.getNodes()!=localNode.getNet().getNodes()){
                    c = true;
                    localNode.getNet().setNodes(remNetwork.getNodes());
                }
                switch (msg.getSteps()){
                    case 1:
                        if (remNetwork.getLeft().equals(localNode.getNet().getLocal())){
                            c = true;
                            localNode.getNet().setRight(remNetwork.getLocal());
                            localNode.getNet().setsRight(remNetwork.getRight());
                        } else {
                            localNode.getNet().setLeft(remNetwork.getLocal());
                            localNode.getNet().setsLeft(remNetwork.getLeft());
                        }break;
                    case 2:
                        if (remNetwork.getsLeft().equals(localNode.getNet().getLocal())){
                            c = true;
                            localNode.getNet().setsRight(remNetwork.getLocal());
                        } else {
                            localNode.getNet().setsLeft(remNetwork.getLocal());
                        }break;
                }
                msg.addStep();
                if (msg.getSteps()>localNode.getNet().getNodes())c =false;
                if (c) localNode.getRemoteSleeper(localNode.getNet().getLeft()).onMessage(msg);
                break;
            case TOKEN:
                System.out.println("got a token");
                if (!black){
                    if (msg.getSteps()>=Node.getInstance().getNet().getNodes()){
                        System.out.println("The task is finished");
                    } else {
                        msg.addStep();
                        Node n = Node.getInstance();
                        n.getRemoteSleeper(getNode().getNet().getLeft()).onMessage(msg);
                    }
                }
        }
    }

    @Override
    public void disconnect() throws RemoteException, NotBoundException {
        try {
            getNode().getRemoteSleeper(getNode().getNet().getRight()).fixAndRing();
        } catch (Exception e) {
            getNode().getRemoteSleeper(getNode().getNet().getsRight()).setLeft(getNode().getNet().getLocal());
            getNode().getRemoteSleeper(getNode().getNet().getsRight()).fixAndRing();
        }
    }

    //communication only to the left > only left can be found missing
    @Override
    public void fixAndRing() throws RemoteException, NotBoundException {
        Node localNode = getNode();
        localNode.getNet().setLeft(localNode.getNet().getsLeft());
        Sleeper remote =localNode.getRemoteSleeper(localNode.getNet().getLeft());
        localNode.getNet().setsLeft(remote.getNode().getNet().getLeft());
        localNode.getNet().setNodes(localNode.getNet().getNodes()-1);
        Message msg = new Message();
        System.out.println("sending 3 rings");
        msg.addStep();
        msg.setType(MessageType.RING);
        msg.setNet(localNode.getNet());
        remote.onMessage(msg);
        localNode.getRemoteSleeper(localNode.getNet().getRight()).onMessage(msg);
        msg.addStep();
        localNode.getRemoteSleeper(localNode.getNet().getsRight()).onMessage(msg);
    }
    @Override
    public void startToken() throws RemoteException, NotBoundException {
        System.out.println("sending a token");
        Message msg = new Message();
        msg.setNet(getNode().getNet());
        msg.addStep();
        msg.setType(MessageType.TOKEN);
        getNode().getRemoteSleeper(getNode().getNet().getLeft()).onMessage(msg);
    }

    private class Timer extends Thread{
        private SleeperImpl sleeper;

        public Timer(SleeperImpl sleeper) {
            this.sleeper = sleeper;
        }

        @Override
        public void run(){
            while (sleeper.work > 0){
                try{Thread.sleep(1000);}catch(InterruptedException e){System.out.println(e);}
                sleeper.work--;
                System.out.println(sleeper.work);
            }
            sleeper.black=false;
            System.out.println("should send a token");
            try {
                sleeper.startToken();
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NotBoundException e) {
                e.printStackTrace();
            }
        }
    }


}
