import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Main {

    public static void main(String[] args) throws NotBoundException, RemoteException{
        String myAddress;
        String remoteAddress;
        Address remote = null;

        if (args.length == 1) {
            myAddress = args[0];
        }
        else if (args.length == 2) {
            myAddress = args[0];
            remoteAddress = args[1];
            remote = new Address(remoteAddress);
        }
        else {
            System.out.println("Usage examples:");
            System.out.println(" 127.0.1.1:2010 (the first node)");
            System.out.println(" 127.0.1.1:2011 127.0.1.1:2010 (Address of the new node and the one we are connecting to)");
            return;
        }

        Node node = Node.getInstance();
        Address local = new Address(myAddress);
        node.getNet().setLocal(local);
        System.setProperty("java.rmi.server.hostname",local.getIp());
        try {
            node.bind();
        }catch (AlreadyBoundException e){

        }
        if (remote != null) {
            try {
                node.connectTo(remote);

            } catch (Exception e) {
                System.out.println("target address is unavalaible");
                return;
            }
        } else {
            try {
                node.connectTo(local);

            } catch (Exception e) {
                System.out.println(e.getMessage());
                return;
            }
        }
        System.out.println("");
        final BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        Sleeper sleeper = node.getRemoteSleeper(node.getNet().getLocal());
        while (true) {
            String line;
            try {
                line = br.readLine();

                if ("quit".equals(line)) {
                    node.disconnect();
                    System.exit(0);
                }
                else if ("start".equals(line)) {
                    try {
                        line = br.readLine();
                        sleeper.start(Integer.parseInt(line));
                    } catch (Exception e){
                        System.out.println("Invalid request");
                    }
                }
                else if ("share".equals(line)) {
                    try {
                        line = br.readLine();
                        sleeper.share(Integer.parseInt(line),node.getNet().getLeft());
                    } catch (Exception e){
                        System.out.println("Invalid request");
                    }
                }
                else if ("request".equals(line)) {
                    try {
                        line = br.readLine();
                        sleeper.request(Integer.parseInt(line),node.getNet().getLeft());
                    } catch (Exception e){
                        System.out.println("Invalid request");
                    }
                } else System.out.println("Invalid request");

            } catch (IOException e) {
                System.out.println("Invalid request");
            }
        }

    }

}
